if (!localStorage.getItem('theme')) {
    localStorage.setItem('theme', 'light')
}

if (localStorage.getItem('theme') === "light") {
    document.querySelector('html').className = 'theme-light';
}

document.querySelector('.onTheme').addEventListener('click',() =>{
    document.querySelector('html').classList.toggle('theme-light')

    localStorage.getItem('theme') === "light" 
    ? localStorage.setItem("theme", "dark") 
    : localStorage.setItem("theme", "light")

})
